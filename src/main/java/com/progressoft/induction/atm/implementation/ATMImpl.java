package com.progressoft.induction.atm.implementation;

import com.progressoft.induction.atm.ATM;
import com.progressoft.induction.atm.BankingSystem;
import com.progressoft.induction.atm.Banknote;
import com.progressoft.induction.atm.data.BankNoteInATM;
import com.progressoft.induction.atm.dto.BankNoteInAtmInfo;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.InvalidAmountException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ATMImpl implements ATM {
    private final BankingSystem bankingSystem = new BankingSystemImpl();
    private final BankNoteInATM bankNoteInATM = new BankNoteInATM();

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        if (amount.remainder(BigDecimal.valueOf(5)).compareTo(BigDecimal.ZERO) == 1)
            throw new InvalidAmountException("Please enter the amount in multiples of 5");
        BigDecimal accountBalance = bankingSystem.getAccountBalance(accountNumber);
        if (accountBalance.compareTo(amount) == -1)
            throw new InsufficientFundsException("Not Enough Balance.");

        //Find number notes equal to amount for withdraw
        List<Banknote> banknotesForGivenAmount =  countNotesEqualToAmountToWithdrawFromAtm(amount);

        bankingSystem.debitAccount(accountNumber,amount);
        return banknotesForGivenAmount;
    }

    /**
     * Compute number of notes for given amount to withdraw and return list of Banknotes equal to given amount
     * @param amount
     * @return List<Banknote>
     */
    private List<Banknote> countNotesEqualToAmountToWithdrawFromAtm(BigDecimal amount) {
        List<Banknote> banknotes = new ArrayList<>();
        List<BankNoteInAtmInfo> bankNoteInAtmInfosList = bankNoteInATM.getAllBankNoteInAtm();
        System.out.println("Amount to withdraw:"+amount);
        if (amount.compareTo(bankNoteInATM.getSumOfAllBankNoteInAtm(bankNoteInAtmInfosList)) == 1)
            throw new NotEnoughMoneyInATMException("Sorry, Not Enough money in ATM.");
        System.out.println("Total amount in ATM:"+bankNoteInATM.getSumOfAllBankNoteInAtm(bankNoteInAtmInfosList));
        getNoteCountForGivenAmount(amount, bankNoteInAtmInfosList, banknotes);
        return banknotes;
    }

    /**
     * Get Notes of all Banknote for the given amount
     * @param amount
     * @param bankNoteInAtmInfosList
     * @param banknotes
     */
    private void getNoteCountForGivenAmount(BigDecimal amount, List<BankNoteInAtmInfo> bankNoteInAtmInfosList, List<Banknote> banknotes) {
        for (BankNoteInAtmInfo bankNoteInAtmInfo : bankNoteInAtmInfosList) {
            //continue counting only if amount is more than or equal to any Note value
            if (amount.compareTo(bankNoteInAtmInfo.getBanknote().getValue()) == 1
                    || amount.compareTo(bankNoteInAtmInfo.getBanknote().getValue()) == 0) {
                int noteCounter = getNoteCounter(amount, banknotes, bankNoteInAtmInfo);//Number of current note
                amount = amount.subtract(BigDecimal.valueOf(noteCounter)
                        .multiply(bankNoteInAtmInfo.getBanknote().getValue()));//Update amount to withdraw with remaining amount
                bankNoteInAtmInfo.setNoteCount(bankNoteInAtmInfo.getNoteCount()-noteCounter); //Update number of notes in ATM
            }
        }
    }

    /**
     * Collect notes for specific Banknote(If available in atm)
     * @param amount
     * @param banknotes
     * @param bankNoteInAtmInfo
     * @return int
     */
    private int getNoteCounter(BigDecimal amount, List<Banknote> banknotes, BankNoteInAtmInfo bankNoteInAtmInfo) {
        int noteCounter = amount.divide(bankNoteInAtmInfo.getBanknote().getValue()).intValue();
        if (noteCounter > bankNoteInAtmInfo.getNoteCount())//if noteCounter exceeds available notes in ATM, re-manage noteCounter
            noteCounter = bankNoteInAtmInfo.getNoteCount();
        for (int i = 0; i<noteCounter; i = i+1) {
            banknotes.add(bankNoteInAtmInfo.getBanknote());//Add notes as much as noteCounter
        }
        return noteCounter;
    }

}
