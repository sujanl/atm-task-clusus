package com.progressoft.induction.atm.implementation;

import com.progressoft.induction.atm.BankingSystem;
import com.progressoft.induction.atm.data.AccountDetails;
import com.progressoft.induction.atm.dto.AccountInfo;
import com.progressoft.induction.atm.exceptions.AccountNotFoundException;

import java.math.BigDecimal;
import java.util.List;

public class BankingSystemImpl implements BankingSystem {
    private final List<AccountInfo> accountInfoList = new AccountDetails().getAccountDetails();

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        if (!isAccountNumberValid(accountNumber))
            throw new AccountNotFoundException("Account not Found.");
        for (AccountInfo accountInfo: accountInfoList){
            if (accountInfo.getAccountNumber().equalsIgnoreCase(accountNumber)) {
                System.out.println("Account Number: "+accountInfo.getAccountNumber()+"\nBalance: "+ accountInfo.getBalance());
                return accountInfo.getBalance();
            }
        }
        return BigDecimal.valueOf(0);
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        for (AccountInfo accountInfo: accountInfoList){
            if (accountInfo.getAccountNumber().equalsIgnoreCase(accountNumber)) {
                accountInfo.setBalance(accountInfo.getBalance().subtract(amount));
                System.out.println("Amount withdrawn:" + amount);
                System.out.println("Total balance in Account after withdraw:"+accountInfo.getBalance());
            }
        }
    }

    /**
     * Check validity of Account number
     * @param accountNumber
     * @return boolean
     */
    private boolean isAccountNumberValid(String accountNumber) {
        for (AccountInfo accountInfo: new AccountDetails().getAccountDetails()){
            if (accountInfo.getAccountNumber().equalsIgnoreCase(accountNumber))
                return true;
        }
        return false;
    }
}
