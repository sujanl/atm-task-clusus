package com.progressoft.induction.atm.dto;

import com.progressoft.induction.atm.Banknote;

public class BankNoteInAtmInfo {
    private Banknote banknote;
    private int noteCount;

    public BankNoteInAtmInfo(Banknote banknote, int noteCount) {
        this.banknote = banknote;
        this.noteCount = noteCount;
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public void setBanknote(Banknote banknote) {
        this.banknote = banknote;
    }

    public int getNoteCount() {
        return noteCount;
    }

    public void setNoteCount(int noteCount) {
        this.noteCount = noteCount;
    }

    @Override
    public String toString() {
        return "TotalBankNoteInAtmInfo{" +
                "banknote=" + banknote +
                ", noteCount=" + noteCount +
                '}';
    }
}
