package com.progressoft.induction.atm.data;

import com.progressoft.induction.atm.dto.AccountInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountDetails {
    AccountInfo accountInfo1 = new AccountInfo("123456789", new BigDecimal("1000.0"));
    AccountInfo accountInfo2 = new AccountInfo("111111111", new BigDecimal("1000.0"));
    AccountInfo accountInfo3 = new AccountInfo("222222222", new BigDecimal("1000.0"));
    AccountInfo accountInfo4 = new AccountInfo("333333333", new BigDecimal("1000.0"));
    AccountInfo accountInfo5 = new AccountInfo("444444444", new BigDecimal("1000.0"));

    public List<AccountInfo> getAccountDetails() {
        List<AccountInfo> accountInfos = new ArrayList<>();
        accountInfos.add(accountInfo1);
        accountInfos.add(accountInfo2);
        accountInfos.add(accountInfo3);
        accountInfos.add(accountInfo4);
        accountInfos.add(accountInfo5);

        return accountInfos;
    }
}
