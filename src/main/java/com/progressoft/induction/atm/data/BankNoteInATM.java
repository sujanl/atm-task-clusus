package com.progressoft.induction.atm.data;

import com.progressoft.induction.atm.Banknote;
import com.progressoft.induction.atm.dto.BankNoteInAtmInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankNoteInATM {
    BankNoteInAtmInfo bankNoteInAtmInfo1 = new BankNoteInAtmInfo(Banknote.FIFTY_JOD,10);
    BankNoteInAtmInfo bankNoteInAtmInfo2 = new BankNoteInAtmInfo(Banknote.TWENTY_JOD,20);
    BankNoteInAtmInfo bankNoteInAtmInfo3 = new BankNoteInAtmInfo(Banknote.TEN_JOD,100);
    BankNoteInAtmInfo bankNoteInAtmInfo4 = new BankNoteInAtmInfo(Banknote.FIVE_JOD,100);

    public List<BankNoteInAtmInfo> getAllBankNoteInAtm() {
        List<BankNoteInAtmInfo> bankNoteInAtmInfoList = new ArrayList<>();
        bankNoteInAtmInfoList.add(bankNoteInAtmInfo1);
        bankNoteInAtmInfoList.add(bankNoteInAtmInfo2);
        bankNoteInAtmInfoList.add(bankNoteInAtmInfo3);
        bankNoteInAtmInfoList.add(bankNoteInAtmInfo4);
        return bankNoteInAtmInfoList;
    }

    public BigDecimal getSumOfAllBankNoteInAtm(List<BankNoteInAtmInfo> bankNoteInAtmInfoList) {
        BigDecimal total = new BigDecimal(0.0);
        for (BankNoteInAtmInfo bankNoteInAtmInfo : bankNoteInAtmInfoList){
            total = total.add(bankNoteInAtmInfo.getBanknote().getValue().multiply(BigDecimal.valueOf(bankNoteInAtmInfo.getNoteCount())));
        }
        return total;
    }
}
