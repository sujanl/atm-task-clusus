package com.progressoft.induction.atm;

import com.progressoft.induction.atm.implementation.ATMImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class AtmApp {
    public static void main(String args[]){
        System.out.println("ATM initiated");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account number: ");
        String accountNumber = scanner.nextLine();
        System.out.println("Enter amount to withdraw: ");
        BigDecimal amount = scanner.nextBigDecimal();

        ATM atm = new ATMImpl();
        List<Banknote> banknotesWithdrawn = atm.withdraw(accountNumber, amount);
        System.out.println("Notes withdrawn: "+ banknotesWithdrawn);
        System.out.println("---------------------End of transaction-----------------");
    }
}
